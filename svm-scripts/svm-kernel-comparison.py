#https://stackabuse.com/implementing-svm-and-kernel-svm-with-pythons-scikit-learn/
    
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"

# Assign colum names to the dataset
colnames = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'Class']

# Read dataset to pandas dataframe
irisdata = pd.read_csv(url, names=colnames)
#irisdata = pd.read_csv('iris.csv', names=colnames)

# PREPROCESS
X = irisdata.drop('Class', axis=1)
y = irisdata['Class']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20)


# kernel paramaters
# -------------------------------------------------------------------------------------------------------------
# 1. Polynomial Kernel - n the case of polynomial kernel, you also have to pass a value for the degree
# parameter of the SVC class. This basically is the degree of the polynomial.

# TRAIN
svclassifier = SVC(kernel='poly', degree=8)
svclassifier.fit(X_train, y_train)

# PREDICT
y_pred = svclassifier.predict(X_test)

# TEST
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))

# -------------------------------------------------------------------------------------------------------------
# 2. Gaussian Kernel
svclassifier = SVC(kernel='rbf')
svclassifier.fit(X_train, y_train)

# PREDICT
y_pred = svclassifier.predict(X_test)

# TEST
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))

# -------------------------------------------------------------------------------------------------------------
# 3. Sigmoid Kernel
svclassifier = SVC(kernel='sigmoid')
svclassifier.fit(X_train, y_train)
y_pred = svclassifier.predict(X_test)
print(confusion_matrix(y_test, y_pred))
print(classification_report(y_test, y_pred))

# If we compare the performance of the different types of kernels we can clearly see that the sigmoid kernel performs the worst.
# This is due to the reason that sigmoid function returns two values, 0 and 1, therefore it is more suitable for binary classification problems.
# However, in our case we had three output classes.
#
# Amongst the Gaussian kernel and polynomial kernel, we can see that Gaussian kernel achieved a perfect 100% prediction rate while
# polynomial kernel misclassified one instance. Therefore the Gaussian kernel performed slightly better. However, there is no hard and fast
# rule as to which kernel performs best in every scenario. It is all about testing all the kernels and selecting the one with the best results on your test dataset.
