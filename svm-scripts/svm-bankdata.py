# https://stackabuse.com/implementing-svm-and-kernel-svm-with-pythons-scikit-learn/
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix

bankdata = pd.read_csv("bill_authentication.csv")
#print(bankdata)
#print(bankdata.shape)
#print(bankdata.head())

# Data preprocessing involves (1) Dividing the data into attributes and labels and (2) dividing the data into training and testing sets.

# (1)
# To divide the data into attributes and labels, execute the following code:
X = bankdata.drop('Class', axis=1)
y = bankdata['Class']
# In the first line of the script above, all the columns of the bankdata dataframe are being stored in the X variable
# except the "Class" column, which is the label column. The drop() method drops this column.
# In the second line, only the class column is being stored in the y variable.
# At this point of time X variable contains attributes while y variable contains corresponding labels.

# (2)
# Luckily, the model_selection library of the Scikit-Learn library contains the train_test_split method
# that allows us to seamlessly divide data into training and test sets.
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20)

# --------------------------------------------------------------------------------------------------------
# TRAINING
# SVC - This class takes one parameter, which is the kernel type.
svclassifier = SVC(kernel='linear')
svclassifier.fit(X_train, y_train)
# The fit method of SVC class is called to train the algorithm on the training data, which is passed as a parameter to the fit method

# PREDICT
y_pred = svclassifier.predict(X_test)

# TEST
print(confusion_matrix(y_test,y_pred))
print(classification_report(y_test,y_pred))