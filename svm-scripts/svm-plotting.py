# https://www.machinecurve.com/index.php/2020/05/03/creating-a-simple-binary-svm-classifier-with-python-and-scikit-learn/
from sklearn.datasets import make_blobs # which allows us to generate the two clusters/blobs of data
from sklearn.model_selection import train_test_split #  which allows us to split the generated dataset into a part for training and a part for testing easily
import numpy as np # for numbers processing
import matplotlib.pyplot as plt # for generating the plot from above
from sklearn import svm
from sklearn.metrics import plot_confusion_matrix, confusion_matrix, classification_report
from mlxtend.plotting import plot_decision_regions

### Generating a dataset
blobs_random_seed = 42 # The random seed for our blobs ensures that we initialize the pseudorandom numbers generator with
# the same start initialization. We need to do this to ensure that varying initializations don’t interfere with our random numbers generation.
# This can be any number

centers = [(0,0), (5,5)] # The centers represent the (X, y) positions of the centers of the blobs we’re generating

cluster_std = 1 # The cluster standard deviation tells us something about how scattered the centers are across the
# two-dimensional mathematical space. It can be set to any number, but the lower, the more condensed the clusters are.

frac_test_split = 0.33 # The fraction of the test split tells us what percentage of our data is used for testing purposes.
# In our case, that’s 33%, or one third of our dataset.

num_features_for_samples = 2 # The number of features for samples tells us the number of classes we wish to generate data for.
# In our case, that’s 2 classes – we’re building a binary classifier.

num_samples_total = 1000 # The number of samples in total tells us the number of samples that are generated in total.
# For educational purposes, we’re keeping the number quite low today, but it can be set to larger numbers if you desire

inputs, targets = make_blobs(n_samples = num_samples_total, centers = centers, n_features = num_features_for_samples, cluster_std = cluster_std)
# we’re calling make_blobs with the configuration options from before. We store its output in the inputs and targets variables,
# which store the features (inputs) and targets (class outcomes), respectively.

X_train, X_test, y_train, y_test = train_test_split(inputs, targets, test_size=frac_test_split, random_state=blobs_random_seed)

plt.scatter(X_train[:,0], X_train[:,1])
plt.title('Linearly separable data')
plt.xlabel('X1')
plt.ylabel('X2')
plt.show()

### Building the SVM classifier
# This primarily involves two main steps:
# 1. Choosing a kernel function – in order to make nonlinear data linearly separable, if necessary. Don’t worry, we’ll explain this next.
# 1. Building our classifier – i.e., writing our code.

clf = svm.SVC(kernel='linear') # Initialize SVM classifier
clf = clf.fit(X_train, y_train)

### Using the SVM to predict new data samples
predictions = clf.predict(X_test)
# After training, it’s wise to evaluate a model with the test set to see how well it performs.
# Today, we’ll do so by means of a confusion matrix, which shows you the correct and wrong predictions in terms of true positives,
# true negatives, false positives and false negatives

matrix = plot_confusion_matrix(clf, X_test, y_test, cmap=plt.cm.Blues, normalize='true')
plt.title('Confusion matrix for our classifier')
plt.show()

#print(confusion_matrix(y_test,predictions))
print(classification_report(y_test,predictions))


### Finding the support vectors of your trained SVM
# Get support vectors
support_vectors = clf.support_vectors_

# Visualize support vectors
plt.scatter(X_train[:,0], X_train[:,1])
plt.scatter(support_vectors[:,0], support_vectors[:,1], color='red')
plt.title('Linearly separable data with support vectors')
plt.xlabel('X1')
plt.ylabel('X2')
plt.show()

### Visualizing the decision boundary
# Plot decision boundary
plot_decision_regions(X_test, y_test, clf=clf, legend=2)
plt.show()