#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.link import TCLink, Intf
from mininet.cli import CLI
from mininet.log import setLogLevel, info
import os

def emptyNet():
    net = Mininet(topo=None, build=False, host = CPULimitedHost, link = TCLink)

    info("*** Adding controller\n")
    net.addController('c0', controller=RemoteController, ip='10.0.0.200',protocol = 'tcp', port=6633)

    info("*** Adding switches\n")
    s2 = net.addSwitch('s2', cls = OVSKernelSwitch)

    info("*** Adding hosts\n")
    h4 = net.addHost('h4', ip='10.0.0.4', cls = Host)
    h5 = net.addHost('h5', ip='10.0.0.5', cls = Host)
    h6 = net.addHost('h6', ip='10.0.0.6', cls = Host)

    info("*** Adding links\n")
    net.addLink(h4, s2)
    net.addLink(h5, s2)
    net.addLink(h6, s2)

    net.build()
    net.addNAT().configDefault()

    info("*** Starting network\n")
    net.start()

    s2.cmd('ovs-vsctl add-port s2 s2-gre1 -- set interface s2-gre1 type=gre options:remote_ip=10.0.0.10')
    s2.cmd('ovs-vsctl add-port s2 s2-gre2 -- set interface s2-gre2 type=gre options:remote_ip=10.0.0.30')
    s2.cmdPrint('ovs-vsctl show')

    # info("\n*** Performing connectivity tests\n")
    # net.pingAll()
    # print("\n")

    CLI(net)
    info("*** Stopping network\n")
    net.stop()
    os.system("sudo mn -c")
    info("*** You've successfully exited mininet\n")

if __name__ == '__main__':
    setLogLevel( 'info' )
    emptyNet()
