#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.link import TCLink, Intf
from mininet.cli import CLI
from mininet.log import setLogLevel, info
import os

def emptyNet():
    net = Mininet(topo=None, build=False, host = CPULimitedHost, link = TCLink)

    info("*** Adding controller\n")
    net.addController('c0', controller=RemoteController, ip='192.168.4.159', protocol = 'tcp', port=6633)

    info("*** Adding switches\n")
    s1 = net.addSwitch('s1', cls = OVSKernelSwitch)
    s2 = net.addSwitch('s2', cls = OVSKernelSwitch)
    s3 = net.addSwitch('s3', cls = OVSKernelSwitch)

    info("*** Adding hosts\n")
    h1 = net.addHost('h1', ip='10.0.0.1', cls = Host)
    h2 = net.addHost('h2', ip='10.0.0.2', cls = Host)
    h3 = net.addHost('h3', ip='10.0.0.3', cls = Host)
    h4 = net.addHost('h4', ip='10.0.0.4', cls = Host)
    h5 = net.addHost('h5', ip='10.0.0.5', cls = Host)
    h6 = net.addHost('h6', ip='10.0.0.6', cls = Host)
    h7 = net.addHost('h7', ip='10.0.0.7', cls = Host)
    h8 = net.addHost('h8', ip='10.0.0.8', cls = Host)
    h9 = net.addHost('h9', ip='10.0.0.9', cls = Host)
    h10 = net.addHost('h10', ip='10.0.0.10', cls = Host)
    h11 = net.addHost('h11', ip='10.0.0.11', cls = Host)
    h12 = net.addHost('h12', ip='10.0.0.12', cls = Host)

    info("*** Adding links\n")
    net.addLink(h1, s1)
    net.addLink(h2, s1)
    net.addLink(h3, s1)
    net.addLink(h4, s1)
    net.addLink(h5, s2)
    net.addLink(h6, s2)
    net.addLink(h7, s2)
    net.addLink(h8, s2)
    net.addLink(h9, s3)
    net.addLink(h10, s3)
    net.addLink(h11, s3)
    net.addLink(h12, s3)
    net.addLink(s1, s2)
    net.addLink(s2, s3)
    #net.addLink(s1, s2, bw = 5, max_queue_size = 500)

    net.build()
    net.addNAT().configDefault()

    info("*** Starting network\n")
    net.start()
    #
    # net.get('c0').start()
    # net.get('s0').start([c0])
    #
    #s1.cmd('ovs-vsctl add-port s1 s1-gre1 -- set interface s1-gre1 type=gre options:remote_ip=10.0.0.20')
    #s1.cmdPrint('ovs-vsctl show')

    # info("\n*** Performing connectivity tests\n")
    # net.pingAll()
    # print("\n")

    CLI(net)
    info("*** Stopping network\n")
    net.stop()
    os.system("sudo mn -c")
    info("*** You've successfully exited mininet\n")

if __name__ == '__main__':
    setLogLevel( 'info' )
    emptyNet()
