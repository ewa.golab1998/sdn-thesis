#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.link import TCLink, Intf
from mininet.cli import CLI
from mininet.log import setLogLevel, info
import os

def emptyNet():
    net = Mininet(topo=None, build=False, host = CPULimitedHost, link = TCLink)

    info("*** Adding controller\n")
    net.addController('c0', controller=RemoteController, ip='10.0.0.200', protocol = 'tcp', port=6633)

    info("*** Adding switches\n")
    s3 = net.addSwitch('s3', cls = OVSKernelSwitch)

    info("*** Adding hosts\n")
    h7 = net.addHost('h7', ip='10.0.0.7', cls = Host)

    info("*** Adding links\n")
    net.addLink(h7, s3)
    #net.addLink(s1, s2, bw = 5, max_queue_size = 500)

    net.build()
    net.addNAT().configDefault()

    info("*** Starting network\n")
    net.start()

    s3.cmd('ovs-vsctl add-port s3 s3-gre1 -- set interface s3-gre1 type=gre options:remote_ip=10.0.0.20')
    s3.cmdPrint('ovs-vsctl show')

    # info("\n*** Performing connectivity tests\n")
    # net.pingAll()
    # print("\n")

    CLI(net)
    info("*** Stopping network\n")
    net.stop()
    os.system("sudo mn -c")
    info("*** You've successfully exited mininet\n")

if __name__ == '__main__':
    setLogLevel( 'info' )
    emptyNet()
