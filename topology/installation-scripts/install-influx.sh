#!/bin/bash

sudo apt-get install curl
wget https://dl.influxdata.com/influxdb/releases/influxdb_1.8.3_amd64.deb
sudo dpkg -i influxdb_1.8.3_amd64.deb
sudo mv -f influxdb.conf /etc/influxdb/influxdb.conf
sudo systemctl start influxdb
sudo systemctl enable influxdb
#curl -XPOST 'http://localhost:8086/query' --data-urlencode 'q=CREATE DATABASE "telegraf"'
#sudo systemctl restart influxdb
