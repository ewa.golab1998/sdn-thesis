#!/bin/bash

sudo apt-get update -y
sudo apt-get install software-properties-common
sudo apt-add-repository universe
sudo apt-get update
sudo apt install python-pip -y
sudo apt-get install git -y
cd /home/ubuntu
git clone https://github.com/faucetsdn/ryu.git
cd ryu
pip install .
