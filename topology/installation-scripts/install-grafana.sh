#!/bin/bash

sudo apt-get install -y apt-transport-https
sudo apt-get install -y software-properties-common wget
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
echo "deb https://packages.grafana.com/oss/deb stable main" |  sudo tee -a /etc/apt/sources.list.d/grafana.list
sudo apt-get update -y
sudo apt-get install grafana -y
#sudo rm -rf /etc/grafana/provisioning/dashboards/sample.yml
#sudo rm -rf /etc/grafana/provisioning/datasources/sample.yml
sudo mv -f dashboards.json /etc/grafana/provisioning/dashboards/dashboards.json
sudo mv -f datasources.yml /etc/grafana/provisioning/datasources/datasources.yml
sudo systemctl start grafana-server
sudo systemctl enable grafana-server
