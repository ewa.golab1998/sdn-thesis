#!/bin/bash

wget https://dl.influxdata.com/telegraf/releases/telegraf_1.15.3-1_amd64.deb
sudo dpkg -i telegraf_1.15.3-1_amd64.deb
sudo mv -f telegraf.conf /etc/telegraf/telegraf.conf
sudo systemctl start telegraf
sudo systemctl enable telegraf
