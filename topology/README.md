### Influx DB

**$ influx -username admin -password "admin"**
```
Connected to http://localhost:8086 version 1.8.2
InfluxDB shell version: 1.8.2
```

**> show databases**
```
name: databases
name
----
_internal
telegraf
```

**> use telegraf**
```
Using database telegraf
```

**> show measurements**

```
name: measurements
name
----
cpu
disk
diskio
kernel
mem
ping
processes
swap
system
```


**> select * from ping**

```
time                
average_response_ms
host   
maximum_response_ms
minimum_response_ms
packets_received
packets_transmitted
percent_packet_loss
result_code standard_
deviation_ms
ttl
url
```

**sprawdzanie pinga** \
telegraf --config /etc/telegraf/telegraf.conf --input-filter ping --test
można jeszcze użyć net - do obserwowania konkretnego interfejsu
https://github.com/influxdata/telegraf/blob/master/plugins/inputs/net/NET_README.md

**Export table to csv** \
`influx -username admin -password "admin" -database telegraf -execute 'select * from ping' -format csv > test.csv`

```influx -username admin -password "admin"
CREATE USER "admin" WITH PASSWORD 'admin' WITH ALL PRIVILEGES```
