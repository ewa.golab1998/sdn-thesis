variable "subnet_id" {
  type = string
  description = "Subnet ID"
}

variable "private_ips" {
  type = list(string)
  description = "Private IPs"
}

variable "security_groups" {
  type = list(string)
  description = "Security group"
}
