resource "aws_network_interface" "network-interface" {
  subnet_id       = var.subnet_id
  private_ips     = var.private_ips
  security_groups = var.security_groups
}

output "interface_id" {
	value = aws_network_interface.network-interface.id
}
