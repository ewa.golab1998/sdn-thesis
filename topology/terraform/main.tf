provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "mininet-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "mininet-vpc"
  }
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.mininet-vpc.id
}

resource "aws_route_table" "route-table" {
  vpc_id = aws_vpc.mininet-vpc.id

  route {
    cidr_block      = "0.0.0.0/0"
    gateway_id      = aws_internet_gateway.gateway.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.gateway.id
  }
}

resource "aws_subnet" "mininet-subnet" {
  vpc_id            = aws_vpc.mininet-vpc.id
  cidr_block        = "10.0.0.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "mininet-subnet"
  }
}

resource "aws_route_table_association" "association" {
  subnet_id       = aws_subnet.mininet-subnet.id
  route_table_id  = aws_route_table.route-table.id
}

resource "aws_security_group" "mininet-security-group" {
  name            = "mininet-security-group"
  vpc_id          = aws_vpc.mininet-vpc.id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ICMP"
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Openflow"
    from_port   = 6633
    to_port     = 6633
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Grafana"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "mininet-security-group"
  }
}

#-------------------------------------------------------------------------------
# Network Interfaces
#-------------------------------------------------------------------------------

module "network-interface-1" {
  source          = "./network-interface"
  subnet_id       = aws_subnet.mininet-subnet.id
  private_ips     = ["10.0.0.10"]
  security_groups = [aws_security_group.mininet-security-group.id]
}

module "network-interface-2" {
  source          = "./network-interface"
  subnet_id       = aws_subnet.mininet-subnet.id
  private_ips     = ["10.0.0.20"]
  security_groups = [aws_security_group.mininet-security-group.id]
}

module "network-interface-3" {
  source          = "./network-interface"
  subnet_id       = aws_subnet.mininet-subnet.id
  private_ips     = ["10.0.0.30"]
  security_groups = [aws_security_group.mininet-security-group.id]
}

module "controller-interface" {
  source          = "./network-interface"
  subnet_id       = aws_subnet.mininet-subnet.id
  private_ips     = ["10.0.0.200"]
  security_groups = [aws_security_group.mininet-security-group.id]
}

#-------------------------------------------------------------------------------
# Elastic IPs
#-------------------------------------------------------------------------------
#

module "eip-controller" {
  source                    = "./elastic-ip"
  network_interface         = module.controller-interface.interface_id
  associate_with_private_ip = "10.0.0.200"
}

module "eip-1" {
  source                    = "./elastic-ip"
  network_interface         = module.network-interface-1.interface_id
  associate_with_private_ip = "10.0.0.10"
}

module "eip-2" {
  source                    = "./elastic-ip"
  network_interface         = module.network-interface-2.interface_id
  associate_with_private_ip = "10.0.0.20"
}

module "eip-3" {
  source                    = "./elastic-ip"
  network_interface         = module.network-interface-3.interface_id
  associate_with_private_ip = "10.0.0.30"
}

#-------------------------------------------------------------------------------
# EC2 Instances
#-------------------------------------------------------------------------------

module "controller" {
  source                = "./controller"
  ami                   = "ami-0817d428a6fb68645"
  instance_type         = "t2.micro"
  availability_zone     = "us-east-1b"
  key_name              = "ec2-key"
  network_interface_id  = module.controller-interface.interface_id
  name                  = "controller"
}

module "h1" {   # skopiować na instancję skrypt DDoS
  source                = "./host"
  ami                   = "ami-0817d428a6fb68645"
  instance_type         = "t2.micro"
  availability_zone     = "us-east-1b"
  key_name              = "ec2-key"
  network_interface_id  = module.network-interface-1.interface_id
  src_file              = "/home/ewa/sdn-thesis/topology/sdn-network/mn-1.py"
  dst_file              = "~/mn-1.py"
  name                  = "mn-vm-1"
}

module "h2" {
  source                = "./host"
  ami                   = "ami-0817d428a6fb68645"
  instance_type         = "t2.micro"
  availability_zone     = "us-east-1b"
  key_name              = "ec2-key"
  network_interface_id  = module.network-interface-2.interface_id
  src_file              = "/home/ewa/sdn-thesis/topology/sdn-network/mn-2.py"
  dst_file              = "~/mn-2.py"
  name                  = "mn-vm-2"
}

module "h3" {  # grafana, telegraf, influxdb
  source                = "./monitoring"
  ami                   = "ami-0817d428a6fb68645"
  instance_type         = "t2.micro"
  availability_zone     = "us-east-1b"
  key_name              = "ec2-key"
  network_interface_id  = module.network-interface-3.interface_id
  src_file              = "/home/ewa/sdn-thesis/topology/sdn-network/mn-3.py"
  dst_file              = "~/mn-3.py"
  name                  = "mn-vm-3"
  #depends_on = [aws_network_interface.example,]
}
