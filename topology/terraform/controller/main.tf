resource "aws_instance" "controller" {
  ami               = var.ami
  instance_type     = var.instance_type
  availability_zone = var.availability_zone
  key_name          = var.key_name

  network_interface {
    device_index          = 0
    network_interface_id  = var.network_interface_id
  }

  tags = {
    Name = var.name
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/installation-scripts/install-ryu.sh"
    destination = "~/install-ryu.sh"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/sdn-network/run-controller.py"
    destination = "~/run-controller.py"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/installation-scripts/install-mininet.sh"
    destination = "~/install-mininet.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x ~/install-ryu.sh",
      "sudo ~/install-ryu.sh",
      "chmod +x ~/install-mininet.sh",
      "sudo ~/install-mininet.sh"
    ]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("/home/ewa/Downloads/ec2-key.pem")
    host        = self.public_ip
  }
}
