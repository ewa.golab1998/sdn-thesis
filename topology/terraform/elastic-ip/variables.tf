variable "network_interface" {
  type = string
  description = "Subnet ID"
}

variable "associate_with_private_ip" {
  type = string
  description = "Association"
}
