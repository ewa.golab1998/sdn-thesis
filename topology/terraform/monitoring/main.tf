resource "aws_instance" "mininet-monitoring" {
  ami               = var.ami
  instance_type     = var.instance_type
  availability_zone = var.availability_zone
  key_name          = var.key_name

  network_interface {
    device_index          = 0
    network_interface_id  = var.network_interface_id
  }

  tags = {
    Name = var.name
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/installation-scripts/install-mininet.sh"
    destination = "~/install-mininet.sh"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/installation-scripts/install-telegraf.sh"
    destination = "~/install-telegraf.sh"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/installation-scripts/install-influx.sh"
    destination = "~/install-influx.sh"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/installation-scripts/install-grafana.sh"
    destination = "~/install-grafana.sh"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/configs/influxdb.conf"
    destination = "~/influxdb.conf"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/configs/telegraf.conf"
    destination = "~/telegraf.conf"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/configs/dashboards.json"
    destination = "~/dashboards.json"
  }

  provisioner "file" {
    source      = "/home/ewa/sdn-thesis/topology/configs/datasources.yml"
    destination = "~/datasources.yml"
  }

  provisioner "file" {
    source      = var.src_file
    destination = var.dst_file
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x ~/install-mininet.sh",
      "sudo ~/install-mininet.sh",
      "chmod +x ~/install-telegraf.sh",
      "sudo ./install-telegraf.sh",
      "chmod +x ~/install-influx.sh",
      "sudo ./install-influx.sh",
      "chmod +x ~/install-grafana.sh",
      "sudo ./install-grafana.sh"
    ]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("/home/ewa/Downloads/ec2-key.pem")
    host        = self.public_ip
  }
}
