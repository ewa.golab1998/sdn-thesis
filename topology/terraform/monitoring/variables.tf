variable "ami" {
  type = string
  description = "AMI number"
}

variable "instance_type" {
  type = string
  description = "Instance type"
}

variable "availability_zone" {
  type = string
  description = "Availability zone"
}

variable "key_name" {
  type = string
  description = "Key name"
}

variable "network_interface_id" {
  type = string
  description = "Network interface ID"
}

variable "src_file" {
  type = string
  description = "File name"
}

variable "dst_file" {
  type = string
  description = "File name"
}

variable "name" {
  type = string
  description = "Name"
}
