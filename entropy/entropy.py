import math
from collections import Counter
import pandas
import csv

def calculate_probability(number_of_occurrences, length):
    return number_of_occurrences / length

def calculate_entropy(data):
    sum = 0
    for i in list(Counter(data).values()):
        p = calculate_probability(i, len(data))
        sum = sum + p * math.log10(p)   # log10 albo log2
    return (-1 * sum)

def entropy():
    colnames = ['protocol', 'srcip', 'dstip', 'srcport', 'dstport']
    data = pandas.read_csv('ddos-traffic.csv', names=colnames)

    srcip = data.srcip.tolist()
    dstip = data.dstip.tolist()
    srcport = data.srcport.tolist()
    dstport = data.srcport.tolist()

    rows = []
    for i in range(len(srcip) - 5):
        rows.append([calculate_entropy(srcip[i:i+5]),
                    calculate_entropy(srcport[i:i+5]),
                    calculate_entropy(dstip[i:i+5]),
                    calculate_entropy(dstport[i:i+5]),])

    for row in rows:
        with open('ddos-entropy.csv', mode='a') as entropy:
            entropy = csv.writer(entropy, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            entropy.writerow(row)


if __name__ == '__main__':
    entropy()



# test -------------------------------------------------------------------------
# data = [1,2,4,2,1,1,5,3,1,1,1,4,4,0,9]
# data = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
data = [1,1,2,2,2,3,3,3,3,3] # 0.45
# data to będą np adresy ip - lista wszystkich adresów
#print(entropy(data))
